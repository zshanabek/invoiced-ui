import React from 'react';
import { Route } from 'react-router';
import { Layout } from './components';

const routes = <Route path='/' component={Layout} />;

export default routes;
