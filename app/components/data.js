const data = [
  { id: 1, name: 'Zack Siri', email: 'zack@codemy.net' },
  { id: 2, name: 'Zhunissali Shanabek', email: 'zshanabek@gmail.com' },
  { id: 3, name: 'Dagar Davletov', email: 'dagar@method.kz' },
];

export default data;
